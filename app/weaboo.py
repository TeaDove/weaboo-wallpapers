#!/usr/bin/python3
import random
import os
import argparse
from pathlib import Path
import curses
from curses import wrapper
import json


HOME = Path(os.path.expanduser("~"))
WEABOO = HOME / "Pictures" / "Wallpapers" / "Weaboo"
NON_WEABOO = HOME / "Pictures" / "Wallpapers" / "Non weaboo"

HELP = """LEFT - previous weaboo image
RIGHT - next weaboo image
DOWN - previous normal imge
UP - next normal image
q  - exit
"""


WP_NOW = Path(os.popen("gsettings get org.cinnamon.desktop.background picture-uri").read().strip().strip("'"))


def ncurses_tui(stdscr):
    stdscr.clear()
    curses.use_default_colors()
    stdscr.addstr(HELP)
    stdscr.refresh()
    global WP_NOW
    while True:
        c = stdscr.getch()
        if c in [ord('q'), 208]:
            break
        elif c == curses.KEY_LEFT:
            WP_NOW = get_prev_image(WEABOO)
        elif c == curses.KEY_RIGHT:
            WP_NOW = get_next_image(WEABOO)
        elif c == curses.KEY_DOWN:
            WP_NOW = get_prev_image(NON_WEABOO)
        elif c == curses.KEY_UP:
            WP_NOW = get_next_image(NON_WEABOO)
        os.system(f"gsettings set org.cinnamon.desktop.background picture-uri 'file://{WP_NOW}'")
        os.system('gsettings set org.cinnamon.desktop.background picture-options zoom')

        


def get_next_image(folder):
    wp_all = list(folder.iterdir())
    if WP_NOW not in wp_all:
        idx_to_use = 0
    else:
        cur_idx = wp_all.index(WP_NOW) 
        idx_to_use = cur_idx + 1 if cur_idx + 1 < len(wp_all) else 0
    wp = wp_all[idx_to_use]
    return wp


def get_prev_image(folder):
    wp_all = list(folder.iterdir())
    if WP_NOW not in wp_all:
        idx_to_use = 0
    else:
        cur_idx = wp_all.index(WP_NOW) 
        idx_to_use = len(wp_all) - 1 if cur_idx == 0 else cur_idx - 1
    wp = wp_all[idx_to_use]
    return wp


def get_random_image(folder):
    wp_all = list(folder.iterdir())
    if WP_NOW in wp_all:
        wp_all.remove(WP_NOW)
    wp = random.choice(wp_all)
    return wp

def main():
    parser = argparse.ArgumentParser(description='Change wallpapers in Gnome, no args for random NON weaboo wallpaper')
    parser.add_argument('-s', '--seq', action="store_true", help='Choose images not random, but sequentially')
    parser.add_argument('-w', '--weaboo', action="store_true", help='Choose image from "Weaboo" folder')
    parser.add_argument('-c', '--console', action="store_true", help='Use TUI with arrows to choose image, ignore other args')
    args = parser.parse_args()
    
    wp = None
    if args.console:
        wrapper(ncurses_tui)
    elif args.weaboo:
        if args.seq:
            wp = get_next_image(WEABOO)
        else:
            wp = get_random_image(WEABOO)
    else:
        if args.seq:
            wp = get_next_image(NON_WEABOO)
        else:
            wp = get_random_image(NON_WEABOO)
    
    if not args.console:
        print(wp.parts[-1])
        os.system(f"gsettings set org.cinnamon.desktop.background picture-uri 'file://{wp}'")
        os.system('gsettings set org.cinnamon.desktop.background picture-options zoom')

if __name__ == "__main__":
    main()
